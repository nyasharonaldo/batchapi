from models.profile import Profile

class Model_Parser():

    def parse_profile(self,data):
        
        id = data[0]
        name = data[1]
        email = data[2]
        password_hash = data[3]
        access_level = data[4]
        API_KEY = data[5]

        return Profile(id, name, email, password_hash, access_level, API_KEY)