from db.IDB import IDB
import mysql.connector

class MySQL(IDB):
    def __init__(self, user, password, host, database):
        self.cnx =  mysql.connector.connect(user=user, password=password,
                              host=host,
                              database=database)

        self.cursor = self.cnx.cursor()

    def getUserProfileById(self,userId):
        query = "SELECT * from users WHERE id="+str(userId)
        self.cursor.execute(query)
        
        for row in self.cursor:
            return row

    def simpleTest(self):
        query = 'SELECT * FROM users'
        return self.cursor.execute(query)
    
    def query(self, query):
        return self.cursor.execute(query)
    def getUserProfiles(self):
        raise NotImplementedError

    def getAccessLevel(self, userId: int):
        raise NotImplementedError
    
    def getAllData(self):
        raise NotImplementedError