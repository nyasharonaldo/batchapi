class IDB:
    def getUserProfileById(self,userId:int):
        raise NotImplementedError

    def getUserProfiles(self):
        raise NotImplementedError

    def getAccessLevel(self, userId: int):
        raise NotImplementedError
    
    def getAllData(self):
        raise NotImplementedError