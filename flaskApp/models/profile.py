import itertools
import json

class Profile:
    def __init__(self, id, name, email, password_hash, access_level, API_KEY):
        self.id = id
        self.name = name
        self.email = email
        self.password_hash = password_hash
        self.access_level = access_level
        self.API_KEY = API_KEY
    
    @classmethod
    def fromdict(cls, d):
        allowed = ('id', 'name', 'email', 'password_hash', 'access_level', 'API_KEY')
        for k, v in d:
            print(k)
            print(v)
        df = {k : v for k, v in d if k in allowed}
        return cls(**df)