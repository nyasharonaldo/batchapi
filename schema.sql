drop database test;
create database test;
use test;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `access_level` int(2) NOT NULL,
  `API_KEY` int(50) NOT NULL,
  PRIMARY KEY (`id`));


INSERT INTO `users` (name, email, password_hash, 
                access_level, API_KEY) 
                
VALUES ('Nyasha', 'nash@gmail.com', 'nash', 1, 4321);

INSERT INTO `users` (name, email, password_hash, 
                access_level, API_KEY) 
                
VALUES ('David', 'dave@gmail.com', 'dave', 0, 1234);